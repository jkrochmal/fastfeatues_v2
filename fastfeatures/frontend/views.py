import requests
from django.http import HttpResponse, response
from publicApi import models
from django.shortcuts import render
from django.contrib.auth import authenticate, login

from django.contrib.auth.decorators import login_required
from .forms import LoginForm

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username = cd['username'],
                                password = cd['password'])
            if user.is_active:
                login(request, user)
                return HttpResponse('Uwierzytelnienie zakończyło się sukcesem')
            else:
                return HttpResponse('Konto zablokowane')
        else:
            return HttpResponse('Nieprawidłowe dane uwierzytelniające')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})

@login_required
def index(request):
    url = "http://127.0.0.1:8000/api/tasks/"
    payload = {}
    headers = {
        'Authorization': 'Basic YWRtaW46YWRtaW4='
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()
    response = response['results']



    # url = self.apiUrl + 'tasks'
    url = "http://127.0.0.1:8000/api/AssistantSkills/"
    payload = {}
    headers = {
        'Authorization': 'Basic YWRtaW46YWRtaW4='
    }

    skills = requests.request("GET", url, headers=headers, data=payload)
    skills = skills.json()
    skills = skills['results']

    return render(request, 'index.html', {'data': response, 'skills':skills})


