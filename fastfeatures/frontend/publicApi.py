import json

import requests
apiUrl = "http://127.0.0.1:8000/api/"
headers = {
        'Authorization': 'Basic YWRtaW46YWRtaW4='
    }
def getTasks():
        # url = self.apiUrl + 'tasks'
    url = "http://127.0.0.1:8000/api/tasks/"
    payload = {}
    headers = {
            'Authorization': 'Basic YWRtaW46YWRtaW4='
        }

    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()
    response = response['results']

    return response


def getAssistantSkills():
        # url = self.apiUrl + 'tasks'
    url = "http://127.0.0.1:8000/api/AssistantSkills/"
    payload = {}
    headers = {
            'Authorization': 'Basic YWRtaW46YWRtaW4='
        }

    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()
    response = response['results']

    return response
