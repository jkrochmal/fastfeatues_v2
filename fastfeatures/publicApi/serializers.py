from django.contrib.auth.models import User, Group
from publicApi.models import Task, TaskStatus, ErrorList, Assistant, AssistantSkill
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField


class UserSerializer(serializers.HyperlinkedModelSerializer):
    # url = serializers.HyperlinkedIdentityField(view_name="user-detail")

    class Meta:
        model = User
        fields = ['id','url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class AssistantSerializer(serializers.HyperlinkedModelSerializer):
    ownerId = PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = Assistant
        fields = ['url','name','ownerId','id','lastActivity']


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    assistant = PrimaryKeyRelatedField(queryset=Assistant.objects.all())
    class Meta:
        model = Task
        fields = ['url','id','assistant', 'createAt','modifiedAt','status']

class TaskStatusSerializer(serializers.HyperlinkedModelSerializer):
    assistantId = PrimaryKeyRelatedField(queryset=Assistant.objects.all())
    requestId = PrimaryKeyRelatedField(queryset=Task.objects.all())

    class Meta:
        model = TaskStatus
        fields = ['referenceId','status', 'createAt','requestId','assistantId']


class ErrorListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ErrorList
        fields = ['description','requestId']

# class FilteredAssistantListSerializer(serializers.AssistantSerializer):
#
#     def to_representation(self, data):
#         data = data.filter(ownerId=self.context['request'].user, edition__hide=False)
#         return super(FilteredAssistantListSerializer, self).to_representation(data)

class AssistantSkillSerializer(serializers.HyperlinkedModelSerializer):

    # assistantId = PrimaryKeyRelatedField(queryset=Assistant.objects.all())
    # assistantId = FilteredAssistantListSerializer(read_only=True)

    class Meta:
        model = AssistantSkill
        fields = ['name','assistantId']
