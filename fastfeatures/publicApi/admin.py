from django.contrib import admin

# Register your models here.
from .models import Task, Assistant, AssistantSkill, ErrorList, TaskStatus

admin.site.register(Task)
admin.site.register(TaskStatus)
admin.site.register(Assistant)
admin.site.register(AssistantSkill)
admin.site.register(ErrorList)

