from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics
from rest_framework import permissions
from publicApi.models import Task, TaskStatus, ErrorList, AssistantSkill, Assistant
from publicApi.serializers import AssistantSerializer, UserSerializer, GroupSerializer, TaskSerializer, TaskStatusSerializer, ErrorListSerializer, AssistantSkillSerializer
from rest_framework import serializers

from django.shortcuts import get_object_or_404



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # filter(user__username=request.user)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]
    def get_queryset(self, *args, **kwargs):
        # The super() function in Python makes class inheritance more manageable and extensible. The function returns a temporary object that allows reference to a parent class by the keyword super.
        #
        # The super() function has two major use cases:
        #
        # To avoid the usage of the super (parent) class explicitly.
        # To enable multiple inheritances.
        return super().get_queryset(*args, **kwargs).filter(username=self.request.user)
    # def get_queryset(self):
    #     queryset = User.objects.filter(username=self.request.user)
    #     return queryset

    # def get_object(self):
    #
    #     return self.request.user

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class TasksViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskStatusViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = TaskStatus.objects.all()
    serializer_class = TaskStatusSerializer
    permission_classes = [permissions.IsAuthenticated]



class ErrorListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = ErrorList.objects.all()
    serializer_class = ErrorListSerializer
    permission_classes = [permissions.IsAuthenticated]

class AssistantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Assistant.objects.all()
    serializer_class = AssistantSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self,*args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(ownerId=self.request.user)


class AssistantSkillsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = AssistantSkill.objects.all()
    serializer_class = AssistantSkillSerializer
    permission_classes = [permissions.IsAuthenticated]

    # def get_queryset(self,*args, **kwargs):
    #     return super().get_queryset(*args, **kwargs).filter(assistantId.ownerId=self.request.user)