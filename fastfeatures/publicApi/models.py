from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Assistant(models.Model):
    name = models.CharField(max_length=200)
    ownerId = models.ForeignKey(User, on_delete=models.CASCADE)
    lastActivity = models.DateTimeField()

class AssistantSkill(models.Model):
    name = models.CharField(max_length=200)
    assistantId = models.ForeignKey(Assistant, on_delete=models.CASCADE)

class ErrorList(models.Model):
    description = models.CharField(max_length=200)
    requestId = models.IntegerField(blank=False)
class Task(models.Model):
    assistant = models.ForeignKey(Assistant, on_delete=models.CASCADE)
    name = models.ForeignKey(AssistantSkill, on_delete=models.CASCADE)
    createAt = models.DateTimeField(blank=False)
    modifiedAt = models.DateTimeField(blank=False)
    status = models.CharField(max_length=200)

class TaskStatus(models.Model):
    referenceId = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    createAt = models.DateTimeField(blank=False)
    requestId = models.ForeignKey(Task, on_delete=models.CASCADE)
    assistantId = models.ForeignKey(Assistant, on_delete=models.CASCADE)
